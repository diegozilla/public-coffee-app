//
//  COFCoordinator.swift
//  CoffeeUI
//
//  Created by Diego on 9/8/21.
//

import Foundation

public protocol COFCoordinator {
  func start()
}
