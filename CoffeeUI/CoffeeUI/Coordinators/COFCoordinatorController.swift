//
//  COFCoordinatorController.swift
//  CoffeeUI
//
//  Created by Diego on 9/8/21.
//

import UIKit

open class COFCoordinatorController: UIViewController, COFCoordinator {
  
  open var currentNavigationController = UINavigationController()
  
  open override func viewDidLoad() {
    super.viewDidLoad()
    start()
  }
  
  open func start() {
    addContainer(viewController: currentNavigationController, usingSafeArea: false)
  }

  
}
