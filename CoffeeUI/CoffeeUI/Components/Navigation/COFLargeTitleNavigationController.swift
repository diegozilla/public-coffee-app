//
//  COFLargeTitleNavigationController.swift
//  CoffeeUI
//
//  Created by Diego on 9/8/21.
//

import UIKit

open class COFLargeTitleNavigationController: UINavigationController {
    
  open override func viewDidLoad() {
    super.viewDidLoad()
    setup()
  }
  
  open func setup() {
    navigationBar.prefersLargeTitles = true
    navigationItem.largeTitleDisplayMode = .automatic
  }
    
}
