//
//  COFFlexView.swift
//  CoffeeUI
//
//  Created by Diego on 8/30/21.
//

import UIKit

open class COFFlexView: UIView {
  
  open var isVertical: Bool { return true }
  
  open var spacing: CGFloat { return COFConstant.Spacing.s1 }
  
  public lazy var stackView: COFStackView = {
    let sv = COFStackView()
    sv.axis = isVertical ? .vertical : .horizontal
    sv.translatesAutoresizingMaskIntoConstraints = false
    sv.spacing = spacing
    return sv
  }()
  
  public lazy var scrollView: UIScrollView = {
    let sv = UIScrollView()
    sv.translatesAutoresizingMaskIntoConstraints = false
    return sv
  }()

  public lazy var layout: COFViewLayoutBuilder = {
    return COFFlexLayout(parentView: self,
                         stackView: stackView,
                         scrollView: scrollView,
                         content: [],
                         isVertical: isVertical)
  }()
  
  public override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }
  
  required public init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  open func setup() {
    layout.build()
  }
  
  public func add(content: UIView, withPadding padding: UIEdgeInsets = .zero) {
    stackView.addArrangedSubview(content, withPadding: padding)
  }
  
}
