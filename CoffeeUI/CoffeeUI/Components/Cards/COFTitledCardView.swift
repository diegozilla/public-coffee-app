//
//  COFTitledCardView.swift
//  CoffeeUI
//
//  Created by Diego on 8/30/21.
//

import UIKit

open class COFTitledCardView: UIView {

  public var stackView: COFStackView = {
    let sv = COFStackView()
    sv.axis = .vertical
    sv.spacing = COFConstant.Spacing.s1
    return sv
  }()
  
  public lazy var titleLabel: COFLabel = {
    let l = COFLabel(font: UIFont.Style.Title.section, color: UIColor.label)
    l.numberOfLines = 0
    return l
  }()
  
  open var padding: UIEdgeInsets {
    return COFConstant.Padding.cardPadding
  }
  
  open var cardTitle: String? {
    return nil
  }
  
  private lazy var layout: COFViewLayoutBuilder = {
    return COFStackLayout(stackView: stackView,
                          content: cardTitle != nil ? [titleLabel] : [],
                          isVertical: true,
                          parentView: self,
                          padding: padding)
  }()
  
  public override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }
  
  required public init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  open func setup() {
    layout.build()
    titleLabel.set(text: cardTitle)
    layer.cornerRadius = COFConstant.Spacing.s1
  }
  
  public func add(content: [UIView]) {
    for item in content {
      stackView.addArrangedSubview(item)
    }
  }
  
}
