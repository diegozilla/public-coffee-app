//
//  COFBinding.swift
//  CoffeeUI
//
//  Created by Diego on 8/2/21.
//

import Foundation

public struct COFBinding<Input> {
    
  public init() {}
  
  private var callback: ((Input) -> Void)?
  
  public mutating func observe<Object: AnyObject> (_ object: Object,
                                                   with callback: @escaping (Object, Input) -> Void) {
      self.callback = { [weak object] input in
          guard let object = object else { return }
          callback(object, input)
      }
  }
  
  public func update(value: Input) {
    self.callback?(value)
  }
    
}
