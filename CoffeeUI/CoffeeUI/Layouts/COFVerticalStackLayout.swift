//
//  COFVerticalStackLayout.swift
//  CoffeeUI
//
//  Created by Diego on 7/13/21.
//

import UIKit

//delete

public class COFVerticalScrollStackLayoutBuilder: COFViewLayoutBuilder {
  
  public var parentView: UIView
  private var content: [UIView]
  private var scrollView: COFVerticalScrollView
  
  public var padding: UIEdgeInsets = .zero
  
  public init(parentView: UIView,
              scrollView: COFVerticalScrollView,
              content: [UIView]) {
    self.parentView = parentView
    self.scrollView = scrollView
    self.content = content
  }
  
  public func build() {
    
    scrollView.translatesAutoresizingMaskIntoConstraints = false
    parentView.addSubview(scrollView)
    parentView.attachToEdges(childView: scrollView, withPadding: padding)

    for view in content {
      scrollView.addArrangedView(view)
    }
  }
  
}

public class COFVerticalStackLayoutBuilder: COFViewLayoutBuilder {
  
  public var parentView: UIView
  private var stackView: UIStackView
  private var content: [UIView]
  
  public var spacing: CGFloat = 10
  public var padding: UIEdgeInsets = .zero
  
  public init(parentView: UIView,
              stackView: UIStackView,
              content: [UIView]) {
    self.parentView = parentView
    self.stackView = stackView
    self.content = content
  }
  
  public func build() {
    
    stackView.translatesAutoresizingMaskIntoConstraints = false
    stackView.axis = .vertical

    
    parentView.addSubview(stackView)
    NSLayoutConstraint.activate([
      stackView.topAnchor.constraint(equalTo: parentView.safeAreaLayoutGuide.topAnchor, constant: padding.top),
      stackView.leadingAnchor.constraint(equalTo: parentView.safeAreaLayoutGuide.leadingAnchor, constant: padding.left),
      stackView.trailingAnchor.constraint(equalTo: parentView.safeAreaLayoutGuide.trailingAnchor, constant: -padding.right),
      stackView.bottomAnchor.constraint(lessThanOrEqualTo: parentView.safeAreaLayoutGuide.bottomAnchor, constant: -padding.bottom)
    ])
    
    for view in content {
      stackView.addArrangedSubview(view)
    }
    
  }
  
}
