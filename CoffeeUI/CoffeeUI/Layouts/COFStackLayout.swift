//
//  COFStackLayout.swift
//  CoffeeUI
//
//  Created by Diego on 8/30/21.
//

import UIKit

public protocol COFStackLayoutBuilder: COFViewLayoutBuilder {
  var stackView: UIStackView { get }
  var content: [UIView] { get set }
  var isVertical: Bool { get }
  var padding: UIEdgeInsets { get }
}

extension COFStackLayoutBuilder {
  public func build() {
    stackView.translatesAutoresizingMaskIntoConstraints = false
    stackView.axis = isVertical ? .vertical : .horizontal
    parentView.addSubview(stackView)
    parentView.attachToEdges(childView: stackView, withPadding: padding)
    
    for item in content {
      stackView.addArrangedSubview(item)
    }
  }
}

public struct COFStackLayout: COFStackLayoutBuilder {
  public var stackView: UIStackView
  public var content: [UIView]
  public var isVertical: Bool
  public var parentView: UIView
  public var padding: UIEdgeInsets
}
