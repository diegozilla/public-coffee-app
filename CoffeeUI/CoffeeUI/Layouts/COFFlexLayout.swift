//
//  COFFlexLayoutView.swift
//  CoffeeUI
//
//  Created by Diego on 8/25/21.
//

import UIKit

public protocol COFFlexLayoutBuilder: COFViewLayoutBuilder {
  var stackView: UIStackView { get }
  var scrollView: UIScrollView { get }
  var content: [UIView] { get set }
  var isVertical: Bool { get }
}

extension COFFlexLayoutBuilder {
  
  public func build() {
    
    scrollView.translatesAutoresizingMaskIntoConstraints = false
    stackView.translatesAutoresizingMaskIntoConstraints = false
    stackView.axis = isVertical ? .vertical : .horizontal
    
    parentView.addSubview(scrollView)
    scrollView.addSubview(stackView)
    
    parentView.attachToEdges(childView: scrollView)
    scrollView.attachToEdges(childView: stackView)
    
    if isVertical {
      stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
    } else {
      stackView.heightAnchor.constraint(equalTo: scrollView.heightAnchor).isActive = true
    }
    
    for view in content {
      stackView.addArrangedSubview(view)
    }
  }
  
}

public struct COFFlexLayout: COFFlexLayoutBuilder {
  public var parentView: UIView
  public var stackView: UIStackView
  public var scrollView: UIScrollView
  public var content: [UIView]
  public var isVertical: Bool
}
