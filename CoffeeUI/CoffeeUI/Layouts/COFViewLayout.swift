//
//  COFViewLayout.swift
//  CoffeeUI
//
//  Created by Diego on 7/13/21.
//

import UIKit

public protocol COFViewLayoutBuilder {
  var parentView: UIView { get }
  func build()
}

