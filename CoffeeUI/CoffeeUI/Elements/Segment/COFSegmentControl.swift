//
//  COFSegmentControl.swift
//  CoffeeUI
//
//  Created by Diego on 9/8/21.
//

import UIKit

public class COFSegmentControl: COFLabeledInputView {

  public var onTap = COFBinding<Int>()
  
  private lazy var segmentControl: UISegmentedControl = {
    return UISegmentedControl()
  }()
  
  public override var contentView: UIView {
    return segmentControl
  }

  public init(title: String, options: [String]) {
    super.init(frame: .zero)
    self.title = title
    for (index, option) in options.enumerated() {
      segmentControl.insertSegment(withTitle: option, at: index, animated: false)
    }
  }
  
  required public init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func onOptionTapped(action: UIAction) {
    let index = segmentControl.segmentIndex(identifiedBy: action.identifier)
    onTap.update(value: index)
  }
  
}
