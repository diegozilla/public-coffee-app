//
//  COFStackView.swift
//  CoffeeUI
//
//  Created by Diego on 8/26/21.
//

import UIKit

open class COFStackView: UIStackView {

  private var _containerViews: [COFContainerView] = []
  
  public var containerViews: [UIView] {
    return _containerViews.compactMap { $0.content }
  }
  
  public func addArrangedSubview(_ view: UIView, withPadding padding: UIEdgeInsets) {
    let containerView = COFContainerView(content: view, padding: padding)
    _containerViews.append(containerView)
    super.addArrangedSubview(containerView)
  }
  
  open override func addArrangedSubview(_ view: UIView) {
    let containerView = COFContainerView(content: view, padding: .zero)
    _containerViews.append(containerView)
    super.addArrangedSubview(containerView)
  }
  
  open override func removeArrangedSubview(_ view: UIView) {
    guard let index = containerViews.firstIndex(where: { $0 == view }) else { return }
    let containerView = containerViews[index]
    _containerViews.remove(at: index)
    super.removeArrangedSubview(containerView)
  }
  
}
