//
//  COFContainerView.swift
//  CoffeeUI
//
//  Created by Diego on 8/26/21.
//

import UIKit

open class COFContainerView: UIView {
  
  public let content: UIView
  public let padding: UIEdgeInsets
  
  public init(content: UIView, padding: UIEdgeInsets) {
    self.content = content
    self.padding = padding
    super.init(frame: .zero)
    
    addSubview(content)
    content.translatesAutoresizingMaskIntoConstraints = false
    attachToEdges(childView: content, withPadding: padding)
  }
  
  required public init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
