//
//  COFInputView.swift
//  CoffeeUI
//
//  Created by Diego on 9/3/21.
//

import UIKit


open class COFLabeledInputView: COFInputView {
  
  private lazy var titleLabel: COFLabel = {
    let l = COFLabel(font: UIFont.Style.Title.item, color: UIColor.secondaryLabel)
    l.setContentHuggingPriority(.required, for: .horizontal)
    return l
  }()
  
  open var contentView: UIView {
    let placeholderView = UIView()
    placeholderView.backgroundColor = .systemRed
    return placeholderView
  }
  
  public var title: String? {
    get { return titleLabel.text }
    set {
      titleLabel.text = newValue
      titleLabel.accessibilityLabel = newValue
    }
  }
  
  public override init(frame: CGRect) {
    super.init(frame: frame)
    stackView.addArrangedSubview(titleLabel)
    stackView.addArrangedSubview(contentView)
  }
  
  required public init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}


/**
 This class can detect the text size changes the user makes in the device settings.

 Based on the text size value returned through `traitCollectionDidChange` and using the property `.isAccessibilityCategory`, the `UIStackView` will change its axis to vertical or horizontal allowing the content to be displayed in more readable way.
 */
open class COFInputView: UIView {

  internal lazy var stackView: UIStackView = {
    let sv = UIStackView()
    sv.axis = .horizontal
    sv.translatesAutoresizingMaskIntoConstraints = false
    return sv
  }()
  
  public override init(frame: CGRect) {
    super.init(frame: frame)
    backgroundColor = .tertiarySystemGroupedBackground
    layer.cornerRadius = COFConstant.base
    addSubview(stackView)
    attachToEdges(childView: stackView, withPadding: UIEdgeInsets.allSides(value: COFConstant.base))
    configureView(for: self.traitCollection)
  }
  
  required public init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  open override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
    super.traitCollectionDidChange(previousTraitCollection)
    if previousTraitCollection?.preferredContentSizeCategory != traitCollection.preferredContentSizeCategory {
      configureView(for: traitCollection)
    }
  }
  
  private func configureView(for traitCollection: UITraitCollection) {
    let contentSize = traitCollection.preferredContentSizeCategory
    if contentSize.isAccessibilityCategory {
      stackView.axis = .vertical
      stackView.alignment = .leading
      stackView.spacing = 0
    } else {
      stackView.axis = .horizontal
      stackView.alignment = .center
      stackView.spacing = COFConstant.Spacing.s3
    }
  }
  
}
