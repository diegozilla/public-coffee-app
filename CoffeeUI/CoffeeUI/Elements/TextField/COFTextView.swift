//
//  COFTextArea.swift
//  CoffeeUI
//
//  Created by Diego on 8/2/21.
//

import UIKit

public class COFTextView: UITextView {
  
  private static let height = COFConstant.screenHeight * 0.25
  
  public override init(frame: CGRect, textContainer: NSTextContainer?) {
    super.init(frame: frame, textContainer: textContainer)
    setup()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func setup() {
    font = UIFont.Style.Body.text
    textColor = UIColor.label
    backgroundColor = .tertiarySystemGroupedBackground
    layer.cornerRadius = COFConstant.base
    textContainerInset = COFConstant.Padding.textView
    adjustsFontForContentSizeCategory = true
    heightAnchor.constraint(equalToConstant: COFTextView.height).isActive = true
  }
  
  
  
}
