//
//  COFTextField.swift
//  CoffeeUI
//
//  Created by Diego on 6/20/21.
//

import UIKit

public class COFTextFieldItem: COFLabeledInputView, UITextFieldDelegate {
  
  public init(title: String,
              placeholder: String? = nil,
              value: String?,
              accessibilityLabel: String? = nil) {
    super.init(frame: .zero)
    self.title = title
    self.placeholderText = placeholder
    self.text = value
  }
  
  required public init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  public var text: String? {
    get { return textField.text }
    set { textField.text = newValue }
  }
  
  public var placeholderText: String? {
    get { return textField.placeholder }
    set { textField.attributedPlaceholder = NSAttributedString(string: newValue ?? "",
                                                               attributes: [NSAttributedString.Key.font :
                                                                              UIFont.Style.Body.text]) }
  }
  
  public var font: UIFont? {
    get { return textField.font }
    set { textField.font = newValue }
  }
  
  public var textColor: UIColor? {
    get { return textField.textColor }
    set { textField.textColor = newValue }
  }
  
  public var onTextChange = COFBinding<String?>()
  
  private lazy var textField: UITextField = {
    let tf = UITextField()
    tf.addTarget(self, action: #selector(onTextChange(sender:)), for: .editingChanged)
    tf.delegate = self
    tf.adjustsFontForContentSizeCategory = true
    tf.font = UIFont.Style.Body.text
    return tf
  }()
  
  public override var contentView: UIView {
    return textField
  }
  
  @objc private func onTextChange(sender: UITextField) {
    onTextChange.update(value: sender.text)
  }
  
}
//
//extension COFOldTextField {
//
//  public static func createTextField(title: String, placeholder: String) -> COFOldTextField {
//    let textField = COFOldTextField()
//    textField.title = title
//    textField.placeholderText = placeholder
//    return textField
//  }
//
//}
//
//public class COFOldTextField: UIView, UITextFieldDelegate {
//
//  enum TitleStyle {
//    case hidden
//    case presented
//  }
//
//  private var presentTextAnimator = UIViewPropertyAnimator(duration: 0.25, curve: .easeInOut, animations: nil)
//  private var dismissTextAnimator = UIViewPropertyAnimator(duration: 0.25, curve: .easeInOut, animations: nil)
//
//  private var setup: TitleStyle = .hidden
//  private var isEditing = false
//
//  public var onTextChange = COFBinding<String?>()
//
//  public var text: String? {
//    get { return textField.text }
//    set {
//      isEditing = false
//      textField.text = newValue
//      setup = newValue == nil ? .hidden : .presented
//      updateInnerLayout(isLayoutNeeded: false)
//    }
//  }
//
//  public var placeholderText: String? {
//    get { return textField.placeholder }
//    set { textField.attributedPlaceholder = NSAttributedString(string: newValue ?? "",
//                                                               attributes: [NSAttributedString.Key.font : UIFont.Style.Body.text]) }
//  }
//
//  public var font: UIFont? {
//    get { return textField.font }
//    set { textField.font = newValue }
//  }
//
//  public var textColor: UIColor? {
//    get { return textField.textColor }
//    set { textField.textColor = newValue }
//  }
//
//  public var title: String? {
//    get { return titleLabel.text }
//    set { titleLabel.text = newValue }
//  }
//
//  var textCenterConstraint: NSLayoutConstraint!
//  var textTopConstraint: NSLayoutConstraint!
//  var textHeightConstraint: NSLayoutConstraint!
//
//  private lazy var textField: UITextField = {
//    let tf = UITextField()
//    tf.translatesAutoresizingMaskIntoConstraints = false
//    tf.addTarget(self, action: #selector(onTextChange(sender:)), for: .editingChanged)
//    tf.delegate = self
//    tf.adjustsFontForContentSizeCategory = true
//    tf.font = UIFont.Style.Body.text
//    return tf
//  }()
//
//  private lazy var titleLabel: UILabel = {
//    let l = UILabel()
//    l.translatesAutoresizingMaskIntoConstraints = false
//    l.textColor = UIColor.secondaryLabel
//    l.isHidden = true
//    l.font = UIFont.Style.Title.item
//    l.alpha = 0
//    l.adjustsFontForContentSizeCategory = true
//    return l
//  }()
//
//  public override init(frame: CGRect) {
//    super.init(frame: frame)
//    setUp()
//  }
//
//  required init?(coder: NSCoder) {
//    fatalError("init(coder:) has not been implemented")
//  }
//
//  private func setUp() {
//    addSubview(textField)
//    addSubview(titleLabel)
//    setUpLayout()
//    setUpStyle()
//  }
//
//  private func setUpLayout() {
//
//    textCenterConstraint = textField.centerYAnchor.constraint(equalTo: centerYAnchor)
//    textTopConstraint = textField.topAnchor.constraint(equalTo: titleLabel.bottomAnchor)
//    textHeightConstraint = heightAnchor.constraint(greaterThanOrEqualToConstant:
//                                                    COFConstant.baseValue(withMultiplier: 7.5))
//
//    NSLayoutConstraint.activate([
//      titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 8),
//      titleLabel.leadingAnchor.constraint(equalTo: textField.leadingAnchor),
//      titleLabel.trailingAnchor.constraint(equalTo: textField.trailingAnchor),
//      textCenterConstraint,
//      textHeightConstraint,
//      textField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: COFConstant.base),
//      textField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -COFConstant.base),
//      textField.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8)
//    ])
//  }
//
//  private func setUpStyle() {
//    backgroundColor = .tertiarySystemGroupedBackground
//    layer.cornerRadius = COFConstant.base
//  }
//
//  public func textFieldDidBeginEditing(_ textField: UITextField) {
//    isEditing = true
//  }
//
//  public func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
//    isEditing = false
//  }
//
//  @objc private func onTextChange(sender: UITextField) {
//    if let text = self.text, !text.isEmpty {
//
//      if setup == .hidden {
//        setup = .presented
//        updateInnerLayout()
//      }
//
//    } else {
//
//      if setup == .presented {
//        setup = .hidden
//        updateInnerLayout()
//      }
//    }
//
//    onTextChange.update(value: sender.text)
//  }
//
//  private func setPresenterAnimator() {
//    presentTextAnimator.addAnimations {
//      self.titleLabel.alpha = 1
//      self.layoutIfNeeded()
//    }
//  }
//
//  private func setDismissAnimator() {
//    dismissTextAnimator.addAnimations {
//      self.titleLabel.alpha = 0
//      self.layoutIfNeeded()
//    }
//
//    dismissTextAnimator.addCompletion { _ in
//      self.titleLabel.isHidden = true
//    }
//  }
//
//  private func setConstraintForSingleTextField(_ value: Bool) {
//    textTopConstraint.isActive = !value
//    textCenterConstraint.isActive = value
//    //textHeightConstraint.isActive = value
//  }
//
//  private func updateInnerLayout(isLayoutNeeded: Bool = true) {
//    if isEditing {
//
//      switch setup {
//      case .hidden:
//
//        setDismissAnimator()
//        setConstraintForSingleTextField(true)
//
//        if presentTextAnimator.isRunning {
//          presentTextAnimator.stopAnimation(true)
//        }
//
//        dismissTextAnimator.startAnimation()
//
//      case .presented:
//
//        setPresenterAnimator()
//        setConstraintForSingleTextField(false)
//        self.titleLabel.isHidden = false
//
//        if dismissTextAnimator.isRunning {
//          dismissTextAnimator.stopAnimation(true)
//        }
//
//        presentTextAnimator.startAnimation()
//      }
//
//    } else {
//
//      switch setup {
//      case .hidden:
//        titleLabel.isHidden = true
//        titleLabel.alpha = 0
//        setConstraintForSingleTextField(true)
//      case .presented:
//        titleLabel.isHidden = false
//        titleLabel.alpha = 1
//        setConstraintForSingleTextField(false)
//      }
//
//      if isLayoutNeeded {
//        layoutIfNeeded()
//      }
//
//    }
//  }
//}
//
