//
//  COFFlatButton.swift
//  CoffeeUI
//
//  Created by Diego on 9/8/21.
//

import UIKit

open class COFFlatButton: UIButton {

  public convenience init(title: String, accessibilityLabel: String? = nil) {
    self.init(type: .system)
    self.setTitle(title, for: .normal)
    self.accessibilityLabel = accessibilityLabel == nil ? title : accessibilityLabel
    setup()
  }
  
  public override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }
  
  required public init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func setup() {
    translatesAutoresizingMaskIntoConstraints = false
    setTitleColor(UIColor.white, for: .normal)
    backgroundColor = UIColor.systemBlue
    layer.cornerRadius = COFConstant.base
    titleLabel?.font = UIFont.Style.Title.item
    titleLabel?.adjustsFontForContentSizeCategory = true
    titleLabel?.numberOfLines = 1
    titleLabel?.adjustsFontSizeToFitWidth = true
  }
  
}
