//
//  COFLabel.swift
//  CoffeeUI
//
//  Created by Diego on 7/19/21.
//

import UIKit

open class COFLabel: UILabel {
  
  public init(font: UIFont,
              color: UIColor,
              text: String? = nil,
              accessibilityLabel: String? = nil) {
    super.init(frame: .zero)
    
    self.textColor = color
    self.font = font
    self.accessibilityTraits = .staticText
    self.adjustsFontForContentSizeCategory = true
    self.set(text: text, accessibilityLabel: accessibilityLabel)
  }
  
  required public init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  public func set(text: String?, accessibilityLabel: String? = nil) {
    self.text = text
    self.accessibilityLabel = accessibilityLabel != nil ? accessibilityLabel : text
  }
  
}

extension COFLabel {
  
  public static func section(withTitle title: String) -> COFLabel {
    return COFLabel(font: UIFont.Style.Title.section, color: .label, text: title, accessibilityLabel: title)
  }
  
}
