//
//  UIViewController+Helper.swift
//  CoffeeUI
//
//  Created by Diego on 9/8/21.
//

import UIKit

extension UIViewController {
    
  public func addContainer(viewController childViewController: UIViewController, usingSafeArea useSafeArea: Bool = true) {
    
    addChild(childViewController)
    let childView = childViewController.view!
    view.addSubview(childView)
    
    childView.translatesAutoresizingMaskIntoConstraints = false
    if useSafeArea {
      view.attachToSafeAreaEdges(childView: childView)
    } else {
      view.attachToEdges(childView: childView)
    }
    
    childViewController.didMove(toParent: self)
  }
    
}
