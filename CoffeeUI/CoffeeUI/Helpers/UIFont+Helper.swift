//
//  UIFont+Helper.swift
//  CoffeeUI
//
//  Created by Diego on 7/19/21.
//

import UIKit

extension UIFont {
  
  public static func preferredFont(for style: TextStyle, weight: Weight) -> UIFont {
    let metrics = UIFontMetrics(forTextStyle: style)
    let desc = UIFontDescriptor.preferredFontDescriptor(withTextStyle: style)
    let font = UIFont.systemFont(ofSize: desc.pointSize, weight: weight)
    return metrics.scaledFont(for: font)
  }
}

extension UIFont {
  
  public struct Style {
    
    public struct Title {
      public static var section = UIFont.preferredFont(for: .title2, weight: .bold)
      public static var item = UIFont.preferredFont(for: .callout, weight: .medium)
    }
    
    public struct Body {
      public static var text = UIFont.preferredFont(for: .subheadline, weight: .regular)
    }
    
  }
  
}
