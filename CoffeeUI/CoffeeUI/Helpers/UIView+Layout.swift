//
//  UIView+LayoutHelper.swift
//  CoffeeUI
//
//  Created by Diego on 7/19/21.
//

import UIKit

extension UIView {
  
  public func attachToEdges(childView: UIView, withPadding padding: UIEdgeInsets = .zero) {
    NSLayoutConstraint.activate([
      childView.topAnchor.constraint(equalTo: topAnchor, constant: padding.top),
      childView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: padding.left),
      childView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -padding.right),
      childView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -padding.bottom)
    ])
  }
  
  public func attachToSafeAreaEdges(childView: UIView, withPadding padding: UIEdgeInsets = .zero) {
    NSLayoutConstraint.activate([
      childView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: padding.top),
      childView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: padding.left),
      childView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -padding.right),
      childView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -padding.bottom)
    ])
  }
  
  public func attachToCenter(childView: UIView) {
    NSLayoutConstraint.activate([
      childView.centerXAnchor.constraint(equalTo: centerXAnchor),
      childView.centerYAnchor.constraint(equalTo: centerYAnchor)
    ])
  }
  
}
