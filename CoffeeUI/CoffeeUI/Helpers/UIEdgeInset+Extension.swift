//
//  UIEdgeInset+Extension.swift
//  CoffeeUI
//
//  Created by Diego on 7/19/21.
//

import UIKit

extension UIEdgeInsets {
  
  public static func vertical(top: CGFloat, bottom: CGFloat) -> UIEdgeInsets {
    return UIEdgeInsets(top: top, left: 0, bottom: bottom, right: 0)
  }
  
  public static func horizontal(left: CGFloat, right: CGFloat) -> UIEdgeInsets {
    return UIEdgeInsets(top: 0, left: left, bottom: 0, right: right)
  }
  
  public static func allSides(value: CGFloat) -> UIEdgeInsets {
    return UIEdgeInsets(top: value, left: value, bottom: value, right: value)
  }
  
  public static func top(value: CGFloat) -> UIEdgeInsets {
    return UIEdgeInsets(top: value, left: 0, bottom: 0, right: 0)
  }
  
  public static func left(value: CGFloat) -> UIEdgeInsets {
    return UIEdgeInsets(top: 0, left: value, bottom: 0, right: 0)
  }
  
  public static func bottom(value: CGFloat) -> UIEdgeInsets {
    return UIEdgeInsets(top: 0, left: 0, bottom: value, right: 0)
  }
  
  public static func right(value: CGFloat) -> UIEdgeInsets {
    return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: value)
  }
}

