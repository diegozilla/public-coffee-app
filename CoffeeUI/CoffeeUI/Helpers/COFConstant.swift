//
//  COFConstant.swift
//  CoffeeUI
//
//  Created by Diego on 8/2/21.
//

import UIKit

public struct COFConstant {
  
  public static let screenHeight: CGFloat = UIScreen.main.bounds.height
  public static let screenWidth: CGFloat = UIScreen.main.bounds.width
  
  public static let base: CGFloat = 8
  public static let baseHalf: CGFloat = base * 0.5
  public static let base2: CGFloat = base * 2
  
  public struct Spacing {
    public static let s1: CGFloat = base
    public static let s2: CGFloat = base * 1.5
    public static let s3: CGFloat = base * 2
    public static let s4: CGFloat = base * 2.5
    public static let s5: CGFloat = base * 3
  }
  
  public struct Padding {
    public static let cardPadding = UIEdgeInsets.allSides(value: base2)
    public static let cardOuterPadding = UIEdgeInsets.horizontal(left: base2, right: base2)
    public static let textView = UIEdgeInsets(top: base2, left: base, bottom: base2, right: base)
  }
  
  public static func baseValue(withMultiplier multiplier: CGFloat) -> CGFloat {
    return base * multiplier
  }
  
}
