//
//  COFScene.swift
//  CoffeeUI
//
//  Created by Diego on 6/30/21.
//

import UIKit

public protocol COFSceneType {
  associatedtype SceneViewModel
  
  associatedtype SceneView: UIView & COFViewRenderable where
    SceneView.ViewModel == SceneViewModel,
    SceneViewController == SceneView.Dispatcher
  
  associatedtype SceneViewController: UIViewController & COFViewDispatcher where
    SceneViewController.ViewModel == SceneViewModel,
    SceneViewController.ViewRenderable == SceneView
  
  var initialViewModel: SceneViewModel { get }
  //var view: SceneView { get }
  var viewController: SceneViewController { get }
}

public protocol COFViewDispatcher: AnyObject {
  associatedtype ViewModel
  associatedtype ViewRenderable
  
  var rootView: ViewRenderable { get }
  var viewModel: ViewModel { get set }
}

public protocol COFViewRenderable: AnyObject {
  associatedtype ViewModel
  associatedtype Update
  associatedtype Dispatcher
  
  var dispatcher: Dispatcher? { get }
  func render(viewModel: ViewModel, update: Update)
}

//public struct COFActionListener<Action> {
//
//  public init() { }
//
//  private var callback: ((Action) -> Void)?
//
//  public mutating func observe<Object: AnyObject> (_ object: Object, with callback: @escaping (Object, Action) -> Void) {
//      self.callback = { [weak object] input in
//          guard let object = object else { return }
//          callback(object, input)
//      }
//  }
//
//  public func notify(_ action: Action) {
//    callback?(action)
//  }
//
//}
//
//public protocol COFSceneType {
//
//  associatedtype State
//
//  associatedtype SceneView: UIView & COFUpdatable
//    where SceneView.State == State
//
//  associatedtype SceneViewController: UIViewController & COFUpdater
//    where SceneViewController.RootView == SceneView,
//          SceneViewController.State == State
//
//  associatedtype SceneController: COFSceneController where SceneController.State == State, SceneViewController.Controller == SceneController
//
//  var initialState: State { get }
//  var view: SceneView { get }
//  var viewController: SceneViewController { get }
//  var controller: SceneController { get }
//}
//
//public protocol COFUpdatable: AnyObject {
//
//  associatedtype State
//  associatedtype Action
//
//  var listener: COFActionListener<Action> { get }
//  func update(with state: State)
//}
//
//public protocol COFUpdater: AnyObject {
//
//  associatedtype State
//  associatedtype RootView
//  associatedtype Controller
//
//  var rootView: RootView { get }
//  var controller: Controller? { get }
//  var currentState: State { get set }
//}
//
//public protocol COFSceneController {
//  associatedtype State
//}


