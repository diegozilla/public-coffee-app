//
//  COFAppCoordinator.swift
//  CoffeeApp
//
//  Created by Diego on 9/8/21.
//

import UIKit
import CoffeeUI
import CoffeeScene

class COFAppCoordinator: UIViewController, COFCoordinator {

  private var currentTabBarController = UITabBarController()
  
  private lazy var coffeeCoordinator: COFCoffeeCoordinator = {
    let coordinator =  COFCoffeeCoordinator()
    let iconConfig = UIImage.SymbolConfiguration(scale: .large)
    let gearIcon = UIImage(systemName: "list.dash", withConfiguration: iconConfig)
    coordinator.tabBarItem = UITabBarItem(title: "My Coffee", image: gearIcon, selectedImage: gearIcon)
    return coordinator
  }()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    start()
  }
  
  func start() {
    currentTabBarController.setViewControllers([coffeeCoordinator], animated: false)
    addContainer(viewController: currentTabBarController, usingSafeArea: false)
  }

  
}
