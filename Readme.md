# Coffee App

## Motivation:

I consider myself a big fan of coffee. Not only I love drinking a good cup, I also enjoy the preparing it. Making a good cup of coffee can be a difficult task, because depending on the coffee and the brewing method, you can get different results depending on the preparation.

Some of the variables or factors that can affect a results are:
- Water temperature
- Brewing method
- Grind size of the coffee
- Coffee/Water ratio

Having so many variables means that getting a good coffee will depend on multiple factors, which can be reproduced once they are known but many times there is a lot of experimentation. For example increasing the grind size of certain coffee might increase the sweetness or decreasing the water temperatura might give more acidic flavors.

What I want for this app is to help coffee enthusiasts, as myself, storing their results for each coffee and method they use. This way it will be easier to get a their desired coffee cup and also store the cups they didn't like that much, this way it can be improved next time.

## Architecture:

Currently the app contains 2 Cocoa Touch Frameworks:
- `CoffeeUI`, which contains the UI elements and components that will be used in the app.
- `CoffeeScene`, which is the module where all the coffee related ViewControllers and Views will be implemented.

Additionally there are other modules that will be implemented:
- `RecipesScene`, where the recipes ViewControllers and Views will be implemented.
- `MethodInstrument`, where the methods/instruments ViewControllers and Views will be implement.
- `CoffeeLogic`, where the logic will be implemented (CoreData and other calculations).

The navigation will be implemented through `Coordinators`, although instead of implementing a regular object with the `Coordinator` protocol, the Coordinator will be implemented by a ViewController, which will contain a UINavigationController. The main reason for this is that by using a ViewController it is easier to avoid memory leaks and store the ViewControllers.

The main pattern of the app is MVC, although it will make extensive use of child ViewControllers.

Each child ViewController will have an specific functionality that will allow them to be reused in different ViewControllers and also to be replaced easily if needed. Also having multiple child ViewControllers that compose a main ViewController, will allow the app to avoid Masive View Controllers.

Another key component of the app (although its not implement yet), will be the usage of Operations (GDC) to encapsulate the logic and make it usable in different places of the app. The main advantage of using Operations, is that it can be used easily with GCD, gaining the benefits of multithreading in iOS.

Finally the app is being build with accessibility in mind, so it will use most of the accessibility features available in the iOS platform.