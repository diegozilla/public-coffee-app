//
//  COFCoffeeDescriptionViewController.swift
//  CoffeeScene
//
//  Created by Diego on 8/30/21.
//

import UIKit
import CoffeeUI

class COFCoffeeDescriptionViewController: UIViewController {

  private lazy var rootView: COFCoffeeDescriptionView = {
    return COFCoffeeDescriptionView()
  }()
  
  override func loadView() {
    view = rootView
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }

}
