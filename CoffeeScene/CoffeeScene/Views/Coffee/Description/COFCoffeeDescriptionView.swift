//
//  COFCoffeeDescriptionView.swift
//  CoffeeScene
//
//  Created by Diego on 8/30/21.
//

import UIKit
import CoffeeUI

class COFCoffeeDescriptionView: COFTitledCardView {

  internal lazy var descriptionTextView: COFTextView = {
    return COFTextView()
  }()
  
  override var cardTitle: String? {
    return "Description"
  }

  override func setup() {
    super.setup()
    backgroundColor = .secondarySystemGroupedBackground
    add(content: [descriptionTextView])
  }
  
}
