//
//  COFCoffeeBasicInformationViewController.swift
//  CoffeeScene
//
//  Created by Diego on 8/27/21.
//

import UIKit

class COFCoffeeBasicInformationViewController: UIViewController {

  private lazy var rootView: COFCoffeeBasicInformationView = {
    return COFCoffeeBasicInformationView()
  }()
  
  override func loadView() {
    view = rootView
  }

}
