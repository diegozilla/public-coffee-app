//
//  COFCoffeeBasicInformationView.swift
//  CoffeeScene
//
//  Created by Diego on 8/27/21.
//

import UIKit
import CoffeeUI

class COFCoffeeBasicInformationView: COFTitledCardView {

  internal lazy var nameTextField: COFTextFieldItem = {
    return COFTextFieldItem(title: "Name", value: nil)
  }()
  
  internal lazy var originTextField: COFTextFieldItem = {
    return COFTextFieldItem(title: "Origin", value: nil)
  }()
  
  internal lazy var typeTextField: COFTextFieldItem = {
    return COFTextFieldItem(title: "Type", value: nil)
  }()
  
  internal lazy var roastSegmentControl: COFSegmentControl = {
    return COFSegmentControl(title: "Roast", options: ["Light", "Medium", "Dark"])
  }()
  
  override var cardTitle: String? {
    return "Basic Information"
  }
  
  override func setup() {
    super.setup()
    backgroundColor = .secondarySystemGroupedBackground
    add(content: [nameTextField,
                  originTextField,
                  typeTextField,
                  roastSegmentControl])
  }

}
