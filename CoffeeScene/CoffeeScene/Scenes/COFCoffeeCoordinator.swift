//
//  CofffeeCoordinator.swift
//  CoffeeScene
//
//  Created by Diego on 9/8/21.
//

import CoffeeUI
import UIKit

public class COFCoffeeCoordinator: COFCoordinatorController {

  public override func viewDidLoad() {
    super.viewDidLoad()

  }
  
  public override func start() {
    currentNavigationController = COFLargeTitleNavigationController()
    let initialViewController = COFCoffeeListViewController()
    initialViewController.coordinator = self
    currentNavigationController.setViewControllers([initialViewController], animated: false)
    super.start()
  }
  
}

extension COFCoffeeCoordinator: COFCoffeeListViewControllerCoordinator {
  
  func onAddCoffeeTapped() {
    let viewController = COFCreateCoffeeViewController()
    viewController.coordinator = self
    currentNavigationController.pushViewController(viewController, animated: true)
  }
  
}

extension COFCoffeeCoordinator: COFCreateCoffeeViewControllerCoordinator {
  
}
