//
//  COFCreateCoffeeViewController.swift
//  CoffeeScene
//
//  Created by Diego on 8/27/21.
//

import UIKit
import CoffeeUI

protocol COFCreateCoffeeViewControllerCoordinator: AnyObject {
  
}

public class COFCreateCoffeeViewController: UIViewController {

  weak var coordinator: COFCreateCoffeeViewControllerCoordinator?
  
  private lazy var rootView: COFCreateCoffeeView = {
    return COFCreateCoffeeView()
  }()
  
  private lazy var basicInformationViewController: COFCoffeeBasicInformationViewController = {
    return COFCoffeeBasicInformationViewController()
  }()
  
  private lazy var descriptionViewController: COFCoffeeDescriptionViewController = {
    return COFCoffeeDescriptionViewController()
  }()
  
  public override func loadView() {
    view = rootView
  }
  
  public override func viewDidLoad() {
    super.viewDidLoad()

    setupComponents()
    
    title = "Add coffee"
    navigationItem.largeTitleDisplayMode = .never
  }
  
  private func setupComponents() {
    addChild(basicInformationViewController)
    rootView.add(content: basicInformationViewController.view!, withPadding: COFConstant.Padding.cardOuterPadding)
    basicInformationViewController.didMove(toParent: self)
    
    addChild(descriptionViewController)
    rootView.add(content: descriptionViewController.view!, withPadding: COFConstant.Padding.cardOuterPadding)
    descriptionViewController.didMove(toParent: self)
    
    rootView.addActionButton()
  }
  
}
