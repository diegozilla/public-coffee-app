//
//  COFCreateCoffeeView.swift
//  CoffeeScene
//
//  Created by Diego on 8/27/21.
//

import UIKit
import CoffeeUI

class COFCreateCoffeeView: COFFlexView {

  private lazy var actionButton: UIButton = {
    return COFFlatButton(title: "Add Coffee")
  }()
  
  override func setup() {
    super.setup()
    backgroundColor = .systemGroupedBackground
    scrollView.contentInset.top = COFConstant.Spacing.s3
  }
  
  internal func addActionButton() {
    actionButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    add(content: actionButton, withPadding: COFConstant.Padding.cardOuterPadding)
  }

}
