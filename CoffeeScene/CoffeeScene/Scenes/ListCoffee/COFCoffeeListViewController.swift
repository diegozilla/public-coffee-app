//
//  COFCoffeeListViewController.swift
//  CoffeeScene
//
//  Created by Diego on 9/8/21.
//

import UIKit

protocol COFCoffeeListViewControllerCoordinator: AnyObject {
  func onAddCoffeeTapped()
}

public class COFCoffeeListViewController: UIViewController {
  
  weak var coordinator: COFCoffeeListViewControllerCoordinator?
  
  private lazy var rootView: COFCoffeeListView = {
    return COFCoffeeListView()
  }()
  
  public override func loadView() {
    view = rootView
  }
  
  public override func viewDidLoad() {
    super.viewDidLoad()
    title = "Coffee List"
    navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add,
                                                        target: self,
                                                        action: #selector(onAddCoffeeTapped))
  }
  
  @objc func onAddCoffeeTapped() {
    self.coordinator?.onAddCoffeeTapped()
  }
  
}
